#pragma once

#include "Image.h"
#include "Area.h"
#include <cstdint>
#include <fstream>
#include <future>
#include <string>
#include <sys/types.h>
#include <tuple>
#include "Vector2.h"

class Encoder {
  public:
	uint16_t imageWidth;
	uint16_t imageHeight;
	uint64_t frameCount;

	Encoder(uint16_t imageWidth, uint16_t imageHeight);
	void encode(const char* input, const char* output);

  private:
	const uint8_t MAGIC_BYTES[4] = { 94, 177, 195, 8 };
	const int MAX_MACROBLOCK_DIFF = 5000;
	const uint16_t MACROBLOCK_SIZE = 30;
	static const uint8_t LITERAL_BLOCK = 0;
	static const uint8_t REFERENCE_BLOCK = 3;
	static const int MAX_TRANSLATION = 8;
	static const int MAX_NEG_TRANSLATION = -8;
	Vector2 _precalculatedTranslations[(MAX_TRANSLATION * 2 + 1) * (MAX_TRANSLATION * 2 + 1)];
	void testArea(std::promise<Area>&& p, Image& oldImage, Image& currentImage, Area* testArea);
	void updateSimulation(Image& sumlationImage, Image& currentImage, Image& oldImage, Area& area);
	void generateDebug(Image& debugImage, Area& area);
	void writeArea(std::ofstream& output, Image& currentImage, Area& area);
	bool fileExists(const std::string& name);
	int diffAreaWithOffset(Image& img1, Image& img2, Area& area, int offsetX, int offsetY);
	int diffArea(Image& img1, Image& img2, Area& area);
	Image readFrame(std::ifstream& input, int pixelCount);
	std::tuple<int, int, int> calculateBestTranslationSource(Image& img1, Image& img2, Area& area);
};