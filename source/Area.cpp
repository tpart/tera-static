#include "Area.h"
#include "Vector2.h"
#include <iostream>
#include <tuple>

Area::Area() {
	this->startX = 0;
	this->startY = 0;
	this->endX = 0;
	this->endY = 0;
	this->bestTranslation = Vector2();
}

Area::Area(int startX, int startY, int endX, int endY) {
	this->startX = startX;
	this->startY = startY;
	this->endX = endX;
	this->endY = endY;
	this->bestTranslation = Vector2();
}

uint Area::getSizeX() {
	return endX - startX;
}

uint Area::getSizeY() {
	return endY - startY;
}

void Area::printArea() {
	std::cout << "Area: (" << startX << ", " << startY << ") to (" << endX << ", " << endY << ")\n";
}