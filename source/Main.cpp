#include "Encoder.h"
#include <iostream>
using namespace std;

const uint16_t imageWidth = 1920;
const uint16_t imageHeight = 1080;

int main() {
	Encoder encoder(imageWidth, imageHeight);

	encoder.encode("video.raw", "output.tea");

	return 0;
}