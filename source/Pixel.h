#pragma once
#include <cstdint>
#include <tuple>

class Pixel {
  public:
	uint8_t r;
	uint8_t g;
	uint8_t b;

	Pixel(uint8_t r, uint8_t g, uint8_t b);
	Pixel();
	char getAverage();
	std::tuple<uint8_t, uint8_t, uint8_t> toBytes();
	int getDiff(Pixel &otherPixel);

  private:
	int fastAbsDiff(uint8_t val, uint8_t val2);
};