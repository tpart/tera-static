#pragma once

#include "Vector2.h"
#include <cstddef>
#include <cstdint>
#include <sys/types.h>
#include<tuple>

class Area {
  public:
    int startX;
    int endX;
    int startY;
    int endY;
    bool replacePixels = false;
    Vector2 bestTranslation;

    Area();
    Area(int startX, int startY, int endX, int endY);
    uint getSizeX();
    uint getSizeY();
    void printArea();
};