#include "Image.h"
#include "Pixel.h"
#include <fstream>
#include <iostream>
using namespace std;

Image::Image(std::vector<Pixel> pixels) {
	this->pixels = pixels;
}

Image::Image() {
}

Pixel& Image::getPixel(uint x, uint y, uint16_t& imageWidth) {
	return pixels[y * imageWidth + x];
}

void Image::setPixel(Pixel& newPixel, uint x, uint y, uint16_t& imageWidth) {
	pixels[y * imageWidth + x] = newPixel;
}

void Image::printPixels(int& pixelCount) {
	for (int i = 0; i < pixelCount; i++) {
		cout << "(" << pixels[i].r << ", " << pixels[i].g << ", " << pixels[i].b << ") ";
	}
}

void Image::saveAsFile(const char* outputFile, uint16_t& imageHeight, uint16_t& imageWidth) {
	ofstream output(outputFile);

	output << "P3\n" << imageWidth << ' ' << imageHeight << "\n255\n";

	for (uint y = 0; y < imageHeight; y++) {
		for (uint x = 0; x < imageWidth; x++) {
			Pixel px = getPixel(x, y, imageWidth);
			output << (int)px.r << " " << (int)px.g << " " << (int)px.b << "\n";
		}
	}

	output.close();
}

void Image::drawDebugArea(Area& area, uint16_t& imageHeight, uint16_t& imageWidth) {
	Pixel color(0, 0, 255);
	if (!area.replacePixels) {
		return;
	}
	// Write top line
	for (int x = area.startX; x < area.endX; x++) {
		this->setPixel(color, x, area.startY, imageWidth);
	}
	// Write bottom line
	for (int x = area.startX; x < area.endX; x++) {
		this->setPixel(color, x, area.endY - 1, imageWidth);
	}
	// Write left line
	for (int y = area.startY; y < area.endY; y++) {
		this->setPixel(color, area.startX, y, imageWidth);
	}
	// Write right line
	for (int y = area.startY; y < area.endY; y++) {
		this->setPixel(color, area.endX - 1, y, imageWidth);
	}
}