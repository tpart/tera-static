#include "Encoder.h"
#include "Area.h"
#include "Pixel.h"
#include "Vector2.h"
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>
#include <thread>
#include <future>
#include <sstream>
#include <iomanip>
#include <chrono>
using namespace std;

Encoder::Encoder(uint16_t imageWidth, uint16_t imageHeight) {
	this->imageWidth = imageWidth;
	this->imageHeight = imageHeight;
}

void Encoder::encode(const char* inputFile, const char* outputFile) {
	// Delete output file if output file already exists
	std::remove(outputFile);

	ofstream output(outputFile, ios::binary | ios_base::out);
	ifstream input(inputFile, ios::binary | ios_base::in);

	if (!input.good()) {
		cout << "Could not open file!\n";
		return;
	}

	// Calculate possible translation vectors
	cout << "Calculating possible translation vectors\n";
	int idx = 0;
	for (int offsetY = MAX_NEG_TRANSLATION; offsetY <= MAX_TRANSLATION; offsetY++) {
		for (int offsetX = MAX_NEG_TRANSLATION; offsetX <= MAX_TRANSLATION; offsetX++) {
			_precalculatedTranslations[idx].x = offsetX;
			_precalculatedTranslations[idx].y = offsetY;
			idx++;
		}
	}

	// Get length of file
	input.seekg(0, std::ios::end);
	size_t length = input.tellg();
	input.seekg(0, std::ios::beg);

	// Calculate frame count
	frameCount = length / (imageWidth * imageHeight * 3);

	// Write magic bytes
	output.write((char*)&MAGIC_BYTES, sizeof(MAGIC_BYTES));

	// Write resolution
	char* imageWidthBytes = static_cast<char*>(static_cast<void*>(&imageWidth));
	char* imageHeightBytes = static_cast<char*>(static_cast<void*>(&imageHeight));
	uint16_t macroblockSizeVar = MACROBLOCK_SIZE;
	char* macroblockSizeBytes = static_cast<char*>(static_cast<void*>(&macroblockSizeVar));
	output.write(imageWidthBytes, 2);
	output.write(imageHeightBytes, 2);
	output.write(macroblockSizeBytes, 2);

	// Write frame count
	char* frameCountBytes = static_cast<char*>(static_cast<void*>(&frameCount));
	output.write(frameCountBytes, sizeof(frameCountBytes));

	// Read frames
	int pixelCount = imageWidth * imageHeight;

	// Read first frame
	Image firstImage = readFrame(input, pixelCount);

	// Write first image
	output << LITERAL_BLOCK;
	cout << "Writing first image...\n";

	for (uint y = 0; y < imageHeight; y++) {
		for (uint x = 0; x < imageWidth; x++) {
			Pixel& p = firstImage.getPixel(x, y, imageWidth);
			output.write((char*)&p.r, 1);
			output.write((char*)&p.g, 1);
			output.write((char*)&p.b, 1);
		}
	}

	// Loop through images and write data
	Image& oldImage = firstImage;
	Image currentSimulation = oldImage;

	for (int i = 1; i < frameCount; i++) {
		cout << "Calculating scores for frame " << i << "\n";

		Image currentImage = readFrame(input, pixelCount);

		uint macroblockCount
		        = (imageWidth / MACROBLOCK_SIZE) * (imageHeight / MACROBLOCK_SIZE);

		Area* macroblocksPointers[macroblockCount];

		uint macroblockIdx = 0;
		for (uint y = 0; y < imageHeight; y += MACROBLOCK_SIZE) {
			for (uint x = 0; x < imageWidth; x += MACROBLOCK_SIZE) {
				macroblocksPointers[macroblockIdx] = new Area(x, y, x + MACROBLOCK_SIZE,  y + MACROBLOCK_SIZE);
				macroblockIdx++;
			}
		}

		thread threads[macroblockCount];
		future<Area> futures[macroblockCount];

		for (uint i = 0; i < macroblockCount; i++) {
			Area* ptr = macroblocksPointers[i];
			std::promise<Area> p;
			futures[i] = p.get_future();
			threads[i] = thread(&Encoder::testArea, this, std::move(p), ref(oldImage), ref(currentImage), ptr);
		}

		Image debugImage = currentSimulation;

		for (uint i = 0; i < macroblockCount; i++) {
			threads[i].join();
			Area a = futures[i].get();
			updateSimulation(currentSimulation, currentImage, oldImage, a);
			debugImage.drawDebugArea(a, imageHeight, imageWidth);
			writeArea(output, currentImage, a);
			delete macroblocksPointers[i];
		}

		std::stringstream ss;
		ss << std::setw(6) << std::setfill('0') << i;
		std::string s = ss.str();
		debugImage.saveAsFile(("debug" + s + ".ppm").c_str(), imageHeight, imageWidth);
		currentSimulation.saveAsFile(("preview" + s + ".ppm").c_str(), imageHeight, imageWidth);

		oldImage = currentSimulation;
	}

	output.close();
}

void Encoder::updateSimulation(Image& simulationImage, Image& currentImage, Image& oldImage,
                               Area& area) {
	if (area.replacePixels) {
		// Refresh pixels
		for (uint y = area.startY; y < area.endY; y++) {
			for (uint x = area.startX; x < area.endX; x++) {
				simulationImage.setPixel(currentImage.getPixel(x, y, imageWidth), x, y,
				                         imageWidth);
			}
		}
	}
	else {
		// Move pixels
		Vector2& direction = area.bestTranslation;
		for (int y = area.startY; y < area.endY; y++) {
			for (int x = area.startX; x < area.endX; x++) {
				int localX = x + direction.x;
				int localY = y + direction.y;

				if (localX < 0) {
					localX = 0;
				}
				else if (localX >= imageWidth) {
					localX = imageWidth - 1;
				}
				if (localY < 0) {
					localY = 0;
				}
				else if (localY >= imageHeight) {
					localY = imageHeight - 1;
				}

				simulationImage.setPixel(oldImage.getPixel(localX, localY, imageWidth), x, y,
				                         imageWidth);
			}
		}
	}
}

void Encoder::writeArea(ofstream& output, Image& currentImage, Area& area) {
	if (area.replacePixels) {
		// Write literal block
		output << LITERAL_BLOCK;
		for (uint y = area.startY; y < area.endY; y++) {
			for (uint x = area.startX; x < area.endX; x++) {
				tuple<uint8_t, uint8_t, uint8_t> bytes
				        = currentImage.getPixel(x, y, imageWidth).toBytes();
				output << get<0>(bytes);
				output << get<1>(bytes);
				output << get<2>(bytes);
			}
		}
	}
	else {
		// Write reference block
		output << REFERENCE_BLOCK;
		Vector2& bestTranslation = area.bestTranslation;
		int8_t translationXByte = static_cast<int8_t>(bestTranslation.x);
		int8_t translationYByte = static_cast<int8_t>(bestTranslation.y);
		char* referenceXBytes = static_cast<char*>(static_cast<void*>(&translationXByte));
		output.write(referenceXBytes, 1);
		char* referenceYBytes = static_cast<char*>(static_cast<void*>(&translationYByte));
		output.write(referenceYBytes, 1);
	}
}

void Encoder::testArea(std::promise<Area>&& p, Image& oldImage, Image& currentImage, Area* areaPtr) {
	Area area = *areaPtr;
	if (diffArea(oldImage, currentImage, area) <= MAX_MACROBLOCK_DIFF) {
		// Don't bother checking different translations if no translation is already good enough
		area.bestTranslation = Vector2(0, 0);
		p.set_value(area);
		return;
	}
	tuple<int, int, int> bestAreaTranslation
	        = calculateBestTranslationSource(oldImage, currentImage, area);
	int& diff = get<2>(bestAreaTranslation);
	if (diff > MAX_MACROBLOCK_DIFF) {
		// Pixel update
		area.replacePixels = true;
		p.set_value(area);
		return;
	}
	else {
		// Reference block
		area.bestTranslation
		        = Vector2(get<0>(bestAreaTranslation), get<1>(bestAreaTranslation));
		p.set_value(area);
		return;
	}
}

Image Encoder::readFrame(ifstream& input, int pixelCount) {
	vector<Pixel> pixels;

	for (int pixelIdx = 0; pixelIdx < pixelCount; pixelIdx++) {
		uint8_t r = input.get();
		uint8_t g = input.get();
		uint8_t b = input.get();
		pixels.push_back(Pixel(r, g, b));
	}

	// Add image to vector
	return Image(pixels);
}

tuple<int, int, int> Encoder::calculateBestTranslationSource(Image& img1, Image& img2,
                                                             Area& area) {
	uint arraySize = (MAX_TRANSLATION * 2 + 1) * (MAX_TRANSLATION * 2 + 1);
	int diffs[arraySize];

	int idx = 0;
	for (Vector2 offset : _precalculatedTranslations) {
		diffs[idx] = diffAreaWithOffset(img1, img2, area, offset.x, offset.y);
		idx++;
	}

	// Find smallest diff
	uint smallestIdx = 0;
	int smallestValue = diffs[0];

	// Find smallest index
	for (uint i = 0; i < arraySize; i++) {
		if (smallestValue > diffs[i]) {
			smallestValue = diffs[i];
			smallestIdx = i;
		}
	}

	// cout << "Computed: " << area.getSizeX() << ", " << area.getSizeY() << "; Elapsed time: " << elapsed.count() << " s\n";

	return make_tuple(_precalculatedTranslations[smallestIdx].x,
	                  _precalculatedTranslations[smallestIdx].y, smallestValue);
}

int Encoder::diffArea(Image& img1, Image& img2, Area& area) {
	int diff;

	for (int y = area.startY; y < area.endY; y++) {
		for (int x = area.startX; x < area.endX; x++) {
			Pixel px1 = img1.getPixel(x, y, imageWidth);
			Pixel px2 = img2.getPixel(x, y, imageWidth);

			diff += px1.getDiff(px2);
		}
	}

	return diff;
}

int Encoder::diffAreaWithOffset(Image& img1, Image& img2, Area& area, int offsetX,
                                int offsetY) {
	int diff;

	if (area.startX + offsetX < 0 || area.endX + offsetX >= imageWidth
	    || area.startY + offsetY < 0 || area.endY + offsetY >= imageHeight)
	{
		for (int y = area.startY; y < area.endY; y++) {
			for (int x = area.startX; x < area.endX; x++) {
				int localX = x + offsetX;
				int localY = y + offsetY;

				if (localX < 0) {
					localX = 0;
				}
				else if (localX >= imageWidth) {
					localX = imageWidth - 1;
				}
				if (localY < 0) {
					localY = 0;
				}
				else if (localY >= imageHeight) {
					localY = imageHeight - 1;
				}

				Pixel px1 = img1.getPixel(localX, localY, imageWidth);
				Pixel px2 = img2.getPixel(x, y, imageWidth);

				diff += px1.getDiff(px2);
			}
		}
	}
	else {
		for (int y = area.startY; y < area.endY; y++) {
			for (int x = area.startX; x < area.endX; x++) {
				Pixel px1 = img1.getPixel(x + offsetX, y + offsetY, imageWidth);
				Pixel px2 = img2.getPixel(x, y, imageWidth);

				diff += px1.getDiff(px2);
			}
		}
	}

	return diff;
}