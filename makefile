HEADERS := $(shell find source -name '*.h' -type f)
SOURCES := $(shell find source -name '*.cpp' -type f)
OBJECTS := $(SOURCES:%.cpp=%.o)

tera: $(OBJECTS)
	g++ $^ -o $@

%.o: %.cpp $(HEADERS)
	g++ -c $< -o $@

clean:
	$(RM) $(OBJECTS) tera
	$(RM) preview*
	$(RM) debug*

run: tera
	./tera ${ARGS}